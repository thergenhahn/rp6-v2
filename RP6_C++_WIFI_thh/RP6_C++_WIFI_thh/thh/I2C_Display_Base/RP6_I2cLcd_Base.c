#define RP6

#ifdef RP6
/*****************************************************************************/
// Includes:

#include "RP6RobotBaseLib.h" 	// The RP6 Robot Base Library.
								// Always needs to be included!
								
/*****************************************************************************/// #defines
//#include "RP6I2CslaveTWI.h" // I2C Library Datei einbinden (!!!)

#include "RP6I2CmasterTWI.h"

#define byte uint8_t
#define AbsDelay mSleep
//#define I2C_Write I2CTWI_writeRegisters

#define LcdAdr 0x40                        // I2C Adresse LCD (A0..A2 =Gnd)
#define BlankByte 32

byte LcdZeile[21];

#endif

void LcdCmd(byte, byte);
void put(byte, byte);

#define InL      0x0
#define InH      0x1
#define OutL     0x2
#define OutH     0x3
#define PolInvL  0x4
#define PolInvH  0x5
#define DDR_L    0x6
#define DDR_H    0x7
#define TO_Reg   0x8

//#define CursorOn 0b00001111
//#define CursorOff 0b00001000

#define CursorOn 0b00001111
#define CursorOff 0b00001100

#define Zeilenlaenge 20

#define MaxLcdItems 80          //4 Zeilen a 20 Zeichen
#define DisplayZeile1 0
#define DisplayZeile2 40
#define DisplayZeile3 20
#define DisplayZeile4 60

// Variablen
byte DisplayArray[80];

#ifndef RP6
/*------------------------------------------------------------------------------
Name:           InitZeile
Eingang:        byte InitCharacter
Ausgang:        -
Beschreibung:   L�scht den LCD Ausgabe Puffer
------------------------------------------------------------------------------*/
void InitZeile(byte InitCharacter)
{
    byte i;
    for(i=0; i<Zeilenlaenge-1; i++)
    {
          LcdZeile[i] = InitCharacter;
    }
	LcdZeile[Zeilenlaenge-1] = 0;
}
#endif

/*------------------------------------------------------------------------------
Name:           WriteLcdPufferToDevice
Eingang:        -
Ausgang:        -
Beschreibung:   Schreibt den LCD Ausgabe Puffer auf das LCD
------------------------------------------------------------------------------*/
void WriteLcdPufferToDevice(byte adresse)
{
    byte i;
    for(i=DisplayZeile1; i<DisplayZeile4+20; i++)
    {
          put(adresse, DisplayArray[i]);
    }
}

/*------------------------------------------------------------------------------
Name:           WriteLcdZeile1
Eingang:        byte InitCharacter, byte array Zeile
Ausgang:        -
Beschreibung:   L�scht den LCD Ausgabe Puffer
------------------------------------------------------------------------------*/
void WriteLcdZeile1(byte InitCharacter, byte Zeile[])
{
    byte i;
    byte j;
    j=0;
    for(i=DisplayZeile1; i<DisplayZeile3; i++)
    {
          if(Zeile[j] == 0)
          {
              Zeile[j]= InitCharacter;
          }
          DisplayArray[i] =  Zeile[j];
          j++;
    }
    WriteLcdPufferToDevice(LcdAdr);
}

/*------------------------------------------------------------------------------
Name:           WriteLcdZeile2
Eingang:        byte InitCharacter, byte array Zeile
Ausgang:        -
Beschreibung:   L�scht den LCD Ausgabe Puffer
------------------------------------------------------------------------------*/
void WriteLcdZeile2(byte InitCharacter, byte Zeile[])
{
    byte i;
    byte j;
    j=0;
    for(i=DisplayZeile2; i<DisplayZeile4; i++)
    {
          if(Zeile[j]==0)
          {
              Zeile[j]= InitCharacter;
          }
          DisplayArray[i] =  Zeile[j];
          j++;
    }
    WriteLcdPufferToDevice(LcdAdr);
}

/*------------------------------------------------------------------------------
Name:           WriteLcdZeile3
Eingang:        byte InitCharacter, byte array Zeile
Ausgang:        -
Beschreibung:   L�scht den LCD Ausgabe Puffer
------------------------------------------------------------------------------*/
void WriteLcdZeile3(byte InitCharacter, byte Zeile[])
{
    byte i;
    byte j;
    j=0;
    for(i=DisplayZeile3; i<DisplayZeile2; i++)
    {
          if(Zeile[j]==0)
          {
              Zeile[j]= InitCharacter;
          }
          DisplayArray[i] =  Zeile[j];
          j++;
    }
    WriteLcdPufferToDevice(LcdAdr);
}

/*------------------------------------------------------------------------------
Name:           WriteLcdZeile4
Eingang:        byte InitCharacter, byte array Zeile
Ausgang:        -
Beschreibung:   L�scht den LCD Ausgabe Puffer
------------------------------------------------------------------------------*/
void WriteLcdZeile4(byte InitCharacter, byte Zeile[])
{
    byte i;
    byte j;
    j=0;
    for(i=DisplayZeile4; i<DisplayZeile4+20; i++)
    {
          if(Zeile[j]==0)
          {
              Zeile[j]= InitCharacter;
          }
          DisplayArray[i] =  Zeile[j];
          j++;
    }
    WriteLcdPufferToDevice(LcdAdr);
}

/*------------------------------------------------------------------------------
Name:           LcdPufferInit
Eingang:        byte InitCharacter
Ausgang:        -
Beschreibung:   L�scht den LCD Ausgabe Puffer
------------------------------------------------------------------------------*/
void LcdPufferLoeschen(byte InitCharacter)
{
    byte i;
    for(i=0; i<MaxLcdItems; i++)
    {
          DisplayArray[i] =  InitCharacter;
    }
}


/*------------------------------------------------------------------------------
Name:           LcdZeile1PufferInit
Eingang:        byte InitCharacter
Ausgang:        -
Beschreibung:   L�scht den LCD Ausgabe Puffer
------------------------------------------------------------------------------*/
void LcdZeile1PufferInit(byte InitCharacter)
{
    byte i;
    for(i=DisplayZeile1; i<DisplayZeile3; i++)
    {
          DisplayArray[i] =  InitCharacter;
    }
}

/*------------------------------------------------------------------------------
Name:           LcdZeile2PufferInit
Eingang:        byte InitCharacter
Ausgang:        -
Beschreibung:   L�scht den LCD Ausgabe Puffer
------------------------------------------------------------------------------*/
void LcdZeile2PufferInit(byte InitCharacter)
{
    byte i;
    for(i=DisplayZeile2; i<DisplayZeile4; i++)
    {
          DisplayArray[i] =  InitCharacter;
    }
}

/*------------------------------------------------------------------------------
Name:           LcdZeile3PufferInit
Eingang:        byte InitCharacter
Ausgang:        -
Beschreibung:   L�scht den LCD Ausgabe Puffer
------------------------------------------------------------------------------*/
void LcdZeile3PufferInit(byte InitCharacter)
{
    byte i;
    for(i=DisplayZeile3; i<DisplayZeile2; i++)
    {
          DisplayArray[i] =  InitCharacter;
    }
}

/*------------------------------------------------------------------------------
Name:           LcdZeile4PufferInit
Eingang:        byte InitCharacter
Ausgang:        -
Beschreibung:   L�scht den LCD Ausgabe Puffer
------------------------------------------------------------------------------*/
void LcdZeile4PufferInit(byte InitCharacter)
{
    byte i;
    for(i=DisplayZeile4; i<DisplayZeile4+20; i++)
    {
          DisplayArray[i] =  InitCharacter;
    }
}

/*------------------------------------------------------------------------------
Name:           LcdInit
Eingang:        3Byte I2C Adresse (0..7)
Ausgang:        -
Beschreibung:   Misst Temperatur �ber I2C
------------------------------------------------------------------------------*/
void LcdInit(byte adresse)
{
#ifdef RP6
	I2CTWI_transmit3Bytes(adresse, DDR_L, 0, 0xf0);
#else	
    I2C_Start();            // I2C starten
    I2C_Write(adresse);     // LCD aufrufen
    I2C_Write(DDR_L);       // I/O setzen
    I2C_Write(0);           // P.0- .7:Ausg�nge
    I2C_Write(0xf0);        // P.8-.11:Ausg�nge
    I2C_Stop();             // I2C wird angehalten
#endif	
}

/*------------------------------------------------------------------------------
Name:           LCD Ausgabe
Eingang:        3Byte I2C Adresse
Ausgang:        -
Beschreibung:   Misst Temperatur �ber I2C
------------------------------------------------------------------------------*/
void LCD(byte adresse, byte Wert)
{
#ifdef RP6
	I2CTWI_transmit2Bytes(adresse, OutL, Wert);
	I2CTWI_transmit2Bytes(adresse, OutH, Wert);
#else	
    I2C_Start();        // I2C starten
    I2C_Write(adresse);   // Temperaturmodul mit der Adresse 11111110 aufrufen
    I2C_Write(OutL);
    I2C_Write(Wert);
//    I2C_Stop();         // I2C wird angehalten
//    I2C_Start();        // I2C starten
    I2C_Write(adresse);   // Temperaturmodul mit der Adresse 11111110 aufrufen
    I2C_Write(OutH);
    I2C_Write(Wert);
    I2C_Stop();         // I2C wird angehalten
#endif
}


#ifndef RP6
/*------------------------------------------------------------------------------
Name:           wrcmdbyte
Eingang:        byte cmd, byte states
Ausgang:        -
Beschreibung:   Schreibt ein Kommando byte
------------------------------------------------------------------------------*/
void wrcmdbyte(byte cmd, byte states)
{
 I2C_Write(cmd);//OutL
 I2C_Write(0b11110001 | states);//OutH
 I2C_Write(cmd);//OutL
 I2C_Write(0b11110000 | states);//OutH En
}
#endif

/*------------------------------------------------------------------------------
Name:           ClearDisplay
Eingang:        3Byte I2C Adresse
Ausgang:        -
Beschreibung:   Display l�schen
------------------------------------------------------------------------------*/
void ClearDisplay(byte adresse)
{
    byte states;
    states=0b00000000;
    LcdPufferLoeschen(BlankByte);
	
#ifdef RP6
	byte msgbuffer[6];
	msgbuffer[0] = OutH;
	msgbuffer[1] = 0b00000001;
	msgbuffer[2] = 0b00110000;
	msgbuffer[3] = 0b11110001 | states;
	msgbuffer[4] = 0b00110000;
	msgbuffer[5] = 0b11110000 | states;
	I2CTWI_transmitBytes(adresse, msgbuffer, 6);
    AbsDelay(2);	
	msgbuffer[0] = OutH;
	msgbuffer[1] = 0b00000001;
	msgbuffer[2] = 0b00110000;
	msgbuffer[3] = 0b11110001 | states;
	msgbuffer[4] = 0b00110000;
	msgbuffer[5] = 0b11110000 | states;
	I2CTWI_transmitBytes(adresse, msgbuffer, 6);
    AbsDelay(2);	
	msgbuffer[0] = OutH;
	msgbuffer[1] = 0b00000001;
	msgbuffer[2] = 0b00110000;
	msgbuffer[3] = 0b11110001 | states;
	msgbuffer[4] = 0b00110000;
	msgbuffer[5] = 0b11110000 | states;
	I2CTWI_transmitBytes(adresse, msgbuffer, 6);
    AbsDelay(2);	
	msgbuffer[0] = OutH;
	msgbuffer[1] = 0b00000001;
	msgbuffer[2] = 0b00001111;
	msgbuffer[3] = 0b11110001 | states;
	msgbuffer[4] = 0b00001111;
	msgbuffer[5] = 0b11110000 | states;
	I2CTWI_transmitBytes(adresse, msgbuffer, 6);
    AbsDelay(2);	
	msgbuffer[0] = OutH;
	msgbuffer[1] = 0b00000001;
	msgbuffer[2] = 0b00111100;
	msgbuffer[3] = 0b11110001 | states;
	msgbuffer[4] = 0b00111100;
	msgbuffer[5] = 0b11110000 | states;
	I2CTWI_transmitBytes(adresse, msgbuffer, 6);

    AbsDelay(2);	
	msgbuffer[0] = OutH;
	msgbuffer[1] = 0b00000001;
	msgbuffer[2] = 0b00000001;
	msgbuffer[3] = 0b11110001 | states;
	msgbuffer[4] = 0b00000001;
	msgbuffer[5] = 0b11110000 | states;
	I2CTWI_transmitBytes(adresse, msgbuffer, 6);
    AbsDelay(2);	
	msgbuffer[0] = OutH;
	msgbuffer[1] = 0b00000001;
	msgbuffer[2] = 0b00000110;
	msgbuffer[3] = 0b11110001 | states;
	msgbuffer[4] = 0b00000110;
	msgbuffer[5] = 0b11110000 | states;
	I2CTWI_transmitBytes(adresse, msgbuffer, 6);	
#else	
    I2C_Start();        // I2C starten
    I2C_Write(adresse);   // Temperaturmodul mit der Adresse 11111110 aufrufen
    I2C_Write(OutH);
    I2C_Write(0b00000001);


/*
  wrcmdbyte(0b00111000, states);//Function Set 8 Bit
  AbsDelay(2);
  wrcmdbyte(0b00111000, states);// Function Set 8 Bit
  wrcmdbyte(0b00111000, states);// Function Set 8 Bit
  wrcmdbyte(0b00111000, states);// Function Set 2 Zeilen, 5x7 Punkte
//  wrcmdbyte(0b01001100, states);// Display Control Cursor Off, display On
  wrcmdbyte(0b01001111, states);// Display Control Cursor Off, display On
*/
  wrcmdbyte(0b00110000, states);//Function Set 8 Bit
  AbsDelay(2);
  wrcmdbyte(0b00110000, states);//Function Set 8 Bit
  AbsDelay(2);
  wrcmdbyte(0b001100000, states);// Function Set 8 Bit
  AbsDelay(2);
  wrcmdbyte(0b00001111, states);// Function Set 8 Bit
  AbsDelay(2);
  wrcmdbyte(0b00111100, states);// Display Control Cursor Off, display On
  AbsDelay(2);

  wrcmdbyte(0b00000001, states);// Display Clear
  AbsDelay(2);
  wrcmdbyte(0b00000110, states);// Entry-Mode

    I2C_Stop();         // I2C wird angehalten

//test(adresse);
//  AbsDelay(3000);
//LcdCmd(adresse,0b00010100);
  //AbsDelay(3000);
  //LcdCmd(adresse,0b00010000);
//LcdCmd(adresse,0b01001100);
#endif
}

#ifndef RP6
/*------------------------------------------------------------------------------
Name:           wrdatabyte
Eingang:        byte cmd, byte states
Ausgang:        -
Beschreibung:   Schreibt ein Kommando
------------------------------------------------------------------------------*/
void wrdatabyte(byte cmd, byte states)
{
 I2C_Write(cmd);//OutL
 I2C_Write(0b11110011 | states);//OutH
 I2C_Write(cmd);//OutL
 I2C_Write(0b11110010 | states);//OutH En
}
#endif

/*------------------------------------------------------------------------------
Name:           LcdCursor
Eingang:        byte Mode (true=on, false=off)
Ausgang:        -
Beschreibung:   Schreibt ein Kommando
------------------------------------------------------------------------------*/
void LcdCursor(byte Mode)
{
    if(Mode == true)
    {
       LcdCmd(LcdAdr,CursorOn);
    }
    else
    {
       LcdCmd(LcdAdr,CursorOff);
    }
}

/*------------------------------------------------------------------------------
Name:           cmd
Eingang:        byte cmd, byte states
Ausgang:        -
Beschreibung:   Sendet ein Kommando
------------------------------------------------------------------------------*/
void LcdCmd(byte device, byte cmd)
{
#ifdef RP6
	byte msgbuffer[5];
	msgbuffer[0] = OutL;
	msgbuffer[1] = cmd;
	msgbuffer[2] = 0b11110001;
	msgbuffer[3] = cmd;
	msgbuffer[4] = 0b11110000;
	I2CTWI_transmitBytes(device, msgbuffer, 5);	
#else
    I2C_Start();        // I2C starten
    I2C_Write(device);
    I2C_Write(OutL);
    wrcmdbyte(cmd, 0);
    I2C_Stop();         // I2C wird angehalten
#endif	
}

/*************************************/
/* Zeichen ausgeben                  */
/*************************************/
void put(byte device, byte char_)
{
#ifdef RP6
	byte msgbuffer[5];
	msgbuffer[0] = OutL;
	msgbuffer[1] = char_;
	msgbuffer[2] = 0b11110011;
	msgbuffer[3] = char_;
	msgbuffer[4] = 0b11110010;
	I2CTWI_transmitBytes(device, msgbuffer, 5);	
#else
    I2C_Start();        // I2C starten
    I2C_Write(device);
    I2C_Write(OutL);
    wrdatabyte(char_, 0);
    I2C_Stop();         // I2C wird angehalten if i2c.cstart(Addr[addr[device]])
#endif	
}

void LcdTest(byte adresse)
{
put(adresse, 47);
put(adresse, 48);
put(adresse, 47);
put(adresse, 49);
put(adresse, 47);
put(adresse, 50);
put(adresse, 47);
put(adresse, 51);
put(adresse, 47);
put(adresse, 52);
put(adresse, 47);
put(adresse, 53);
put(adresse, 47);
put(adresse, 54);
put(adresse, 47);
put(adresse, 55);
put(adresse, 47);
put(adresse, 56);
put(adresse, 47);
put(adresse, 57);

put(adresse, 47);
put(adresse, 48);
put(adresse, 47);
put(adresse, 49);
put(adresse, 47);
put(adresse, 50);
put(adresse, 47);
put(adresse, 51);
put(adresse, 47);
put(adresse, 52);
put(adresse, 47);
put(adresse, 53);
put(adresse, 47);
put(adresse, 54);
put(adresse, 47);
put(adresse, 55);
put(adresse, 47);
put(adresse, 56);
put(adresse, 47);
put(adresse, 57);

put(adresse, 65);
put(adresse, 66);
put(adresse, 67);
put(adresse, 68);
put(adresse, 69);
put(adresse, 70);
put(adresse, 71);
put(adresse, 72);
put(adresse, 73);
put(adresse, 74);
put(adresse, 75);
put(adresse, 76);
put(adresse, 77);
put(adresse, 78);
put(adresse, 79);
put(adresse, 80);
put(adresse, 81);
put(adresse, 82);
put(adresse, 83);
put(adresse, 84);
put(adresse, 85);
put(adresse, 86);
put(adresse, 87);
put(adresse, 88);
put(adresse, 89);
put(adresse, 90);
put(adresse, 91);
put(adresse, 92);
put(adresse, 93);
put(adresse, 94);
put(adresse, 95);
put(adresse, 96);
put(adresse, 97);
put(adresse, 98);
//put(adresse, 99);
//LcdCmd(LcdAdr,2);
}