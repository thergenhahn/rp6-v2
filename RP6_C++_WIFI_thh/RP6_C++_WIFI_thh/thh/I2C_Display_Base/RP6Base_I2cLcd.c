/* 
 * ****************************************************************************
 * RP6 ROBOT SYSTEM - ROBOT BASE EXAMPLES
 * ****************************************************************************
 * Example: LEDs and Serial Interface Example
 * Author(s): Dominik S. Herwald
 * ****************************************************************************
 * Description:
 * A typical "Hello World" program for the RP6. Writes text to the PC with the
 * Serial Interface and shows a running Light with the LEDs afterwards.
 *
 * You can watch the text output when RP6 is connected to the USB interface
 * with the Terminal in RP6Loader.
 * In the RP6Loader Software switch to the tab "Terminal" and use the
 * menu item RP6Loader->Start Target or whatever the text is for the 
 * language you selected. 
 * (or press strg+s on your keyboard)
 *
 * Also take a look at "RP6Base_LEDs_uncommented.c"! There you can see how
 * this file looks like without any comments. Much shorter and for some of you
 * maybe better to understand.
 *
 * ############################################################################
 * The Robot does NOT move in this example! You can simply put it on a table
 * next to your PC and you should connect it to the PC via the USB Interface!
 * ############################################################################
 * ****************************************************************************
 */

/*****************************************************************************/
// Includes:
#include "stdio.h"
#include "math.h"
#include "RP6RobotBaseLib.h" 	// The RP6 Robot Base Library.
								// Always needs to be included!
								

#include "RP6I2CmasterTWI.h"
/*****************************************************************************/
// Main function - The program starts here:

extern void LcdTest(uint8_t);
extern void LcdInit(uint8_t);
extern void ClearDisplay(uint8_t);
//extern void InitZeile(uint8_t);
extern void WriteLcdZeile1(uint8_t, char[]);
extern void WriteLcdZeile2(uint8_t, char[]);
extern void WriteLcdZeile3(uint8_t, char[]);
extern void WriteLcdZeile4(uint8_t, char[]);
extern void LcdCursor(uint8_t);

void txdtest(void);

#define Zeilenlaenge 21
#define BlankByte 32
char LcdZeile[Zeilenlaenge];

void txdtest()
{
	writeString_P("Begin1 Init LCD\n");
	LcdInit(0x40);
	task_I2CTWI(); // Call I2C Management routine
	mSleep(2000);
	task_I2CTWI(); // Call I2C Management routine
	writeString_P("End1 Init LCD\n\n");
	
	writeString_P("Begin LCD Test\n");
	LcdTest(0x40);
	task_I2CTWI(); // Call I2C Management routine
	mSleep(2000);
	task_I2CTWI(); // Call I2C Management routine	
	writeString_P("End LCD Test\n\n");
}

void transmissionErrorHandler(uint8_t errorState)
{
	writeString_P("\nI2C ERROR - TWI STATE: 0x");
	writeInteger(errorState, HEX);
	writeChar('\n');
}

void InitZeile(uint8_t InitCharacter)
{
    uint8_t i;
    for(i=0; i<Zeilenlaenge-1; i++)
    {
          LcdZeile[i] = InitCharacter;
    }
	LcdZeile[Zeilenlaenge-1] = 0;
}

void printUBat(uint16_t uBat)
{
	writeIntegerLength((((uBat/102.4f)+0.1f)), DEC, 2);
	writeChar('.');
	writeIntegerLength((((uBat/1.024f)+10)), DEC, 2);
	writeChar('V');
	writeChar('\n');
}

int main(void)
{
	uint16_t ubat;
	uint16_t ubat10;
	uint16_t uADC0;
	uint8_t Flag10ms = 0;
	uint8_t Flag100ms = 0;
	uint16_t Counter100ms = 0;
	
	initRobotBase(); // Always call this first! The Processor will not work
					 // correctly otherwise.

	// ---------------------------------------
	// Write messages to the Serial Interface
	// (here it is a RP6 text logo):
	writeString_P("\n\n   _______________________\n");
	writeString_P("   \\| RP6  ROBOT SYSTEM |/\n");
	writeString_P("    \\_-_-_-_-_-_-_-_-_-_/\n\n");

	// Explanation of special chars:
	// '\n' = new line
	// '\\' = '\'
	// These are "escape sequences" for characters you can not
	// use directly within strings.

	// Write "Hello World" to the Serial Interface:
	writeString_P("Hello World! My name is Robby!\n");
	writeString_P("Let's go! :)\n");

	// ---------------------------------------
	// LEDs:
	setLEDs(0b111111); // Turn all LEDs on!

	// 0b111111 is a binary value and is the same as
	// 63 in the decimal system.
	// For this routine, the binary value is better to read, because each bit
	// represents one of the LEDs.
	// e.g. this:
	// setLEDs(0b000001); would set only LED1
	// setLEDs(0b000010); LED2
	// setLEDs(0b000100); LED3
	// setLEDs(0b101001); LED6, LED4, LED1 - and so on!

	mSleep(1000); // delay 1000ms = 1s
	setLEDs(0b000000); // All LEDs off!
	mSleep(500); // delay 500ms = 0.5s
	
	powerON();
	
	I2CTWI_initMaster(100);
	I2CTWI_setTransmissionErrorHandler(transmissionErrorHandler);
	task_I2CTWI(); // Call I2C Management routine
	
	LcdInit(0x40);
	ClearDisplay(0x40);
	LcdCursor(0);
//	LcdTest(0x40);

	// ---------------------------------------

	uint8_t runningLight = 1; // This defines the local unsigned 8 bit variable "runningLight".
						      // It can be accessed everywhere _below_ in this function.
						      // And ONLY within this function!
	startStopwatch1(); // Stopwatch1 starten!
	setStopwatch2(10); // Stopwatch2 auf 10ms setzen	
	startStopwatch2(); // Stopwatch2 starten!
	setStopwatch2(1000); // Stopwatch2 auf 1000ms setzen	
	// ---------------------------------------
	// Main loop - the program will loop here forever!
	// In this program, it only runs a small LED chaselight.
	while(true)
	{
		if(getStopwatch1() >= 10) // Sind 10ms vergangen?
		{
			setStopwatch1(0); // Stopwatch2 auf 0 zurücksetzen
			Flag10ms = 1;
		}
		if(Flag10ms > 0)
		{
			if(++Counter100ms >= 10)
			{
				Flag100ms = 1;
				Counter100ms = 0;
			}
			Flag10ms = 0;
		}
		
		if(Flag100ms == 1)
		{
			Flag100ms = 0;
		// Here we do a small LED test:
		// ---------------------------------------

			setLEDs(runningLight); 	// Set status LEDs to the value of the variable
							// testLEDs.
							// In the first loop iteration it has the value 1,
							// and thus the StatusLED1 will be switched on.

			runningLight <<= 1; // shift the bits of "runningLight" one step to the left.
						// As there is only one bit set in this variable,
						// only one LED is on at the same time.
						// This results is a moving light dot like this:
						// 1: 0b000001
						// 2: 0b000010
						// 3: 0b000100
						// 4: 0b001000
						// 5: 0b010000
						// 6: 0b100000 
						//
						// In decimal format that would be the numbers:
						// 1, 2, 4, 8, 16, 32

		// When we have reached a value > 32 (32 means Status LED6 is on), 
		// we need to reset the value of runningLight to 1 to start again
		// from the beginning...
		// Instead of "32" we could also write "0b100000".
			if(runningLight > 32)
				runningLight = 1; 	// reset runningLight to 1 (StatusLED1) 

		// If we want to see the running Light, we need to
		// add a delay here - otherwise our human eyes would not see
		// the moving light dot:
			uADC0 = readADC(ADC_ADC0);
			InitZeile(BlankByte);
			sprintf(LcdZeile,"UADC0: %d.%d V",uADC0/100,uADC0- (uADC0/100)*100);
			WriteLcdZeile4(BlankByte,LcdZeile);		
		} 

		if(getStopwatch2() > 1000) // Sind 1000ms (= 1s) vergangen?
		{
			setStopwatch2(0); // Stopwatch2 auf 0 zurücksetzen
			ubat = readADC(ADC_BAT);
			ubat10 = ubat- (ubat/100)*100;
			InitZeile(BlankByte);
//			sprintf(LcdZeile,"UBat: %d0 mV",ubat);
//			WriteLcdZeile1(BlankByte,LcdZeile);
			if(ubat10 < 10)
			{
				sprintf(LcdZeile,"UBat: %d.0%d V",ubat/100,ubat10);
			}
			else
			{
				sprintf(LcdZeile,"UBat: %d.%d V",ubat/100,ubat10);
			}
			WriteLcdZeile2(BlankByte,LcdZeile);
//			LcdCursor(1);
		}
		task_I2CTWI(); // Call I2C Management routine
		
		// ---------------------------------------

	}
	// End of main loop
	// ---------------------------------------

	// ---------------------------------------
	// The Program will NEVER get here!
	// (at least if you don't perform a "break" in the main loop, which
	// you should not do usually!)

	return 0; 
}
