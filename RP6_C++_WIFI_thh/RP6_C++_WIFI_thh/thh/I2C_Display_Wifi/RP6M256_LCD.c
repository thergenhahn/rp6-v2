/* 
 * ****************************************************************************
 * RP6 ROBOT SYSTEM - RP6 CONTROL M256 Examples
 * ****************************************************************************
 * Example: LEDs, LCD, Serial and WIFI Interface Example
 * Author(s): Dominik S. Herwald
 * ****************************************************************************
 * Description:
 * A more complex "Hello World" program for RP6 Control M256 than the one that
 * you should know from RP6 Robot Base. 
 * It writes text to the PC with the serial and WIFI interfaces, shows a 
 * running light with the LEDs, shows a moving Text on the LCD (if available).
 *
 * It shows how to use the Stopwatch feature of the RP6M256Lib, which
 * is nearly the same as with RP6Libary.
 *
 * You can watch the output in BOTH Terminals, Serial and Network/WiFi. 
 *
 *
 * ATTENTION:
 * This and all other Examples for RP6-M256 assume that you did run the other 
 * example programs from the RP6 Robot Base before, understood those 
 * examples (most of it at least) and that you have read the RP6-BASE and 
 * RP6-M256 manuals. Some things are repeated here, but not everything!
 *
 * ############################################################################
 * The Robot does NOT move in this example! You can simply put it on a table
 * next to your PC and you should connect it to the PC via the USB Interface!
 * You should also connect to it via WIFI.
 * ############################################################################
 * ****************************************************************************
 */

/*****************************************************************************/
// Includes:
#include "stdio.h"
#include "RP6M256_WIFIlib.h"

//#include <avr/io.h>
//#include "RP6M256Lib.h" 					// The RP6 Control Library. 
											// Always needs to be included!
#include "RP6I2CmasterTWI.h"	// I2C Bus Master routines

extern void GetHttpParam(char*, char*, char*);

/*****************************************************************************/
extern void LcdTest(uint8_t);
extern void LcdInit(uint8_t);
extern void ClearDisplay(uint8_t);
//extern void InitZeile(uint8_t);
extern void WriteLcdZeile1(uint8_t, char[]);
extern void WriteLcdZeile2(uint8_t, char[]);
extern void WriteLcdZeile3(uint8_t, char[]);
extern void WriteLcdZeile4(uint8_t, char[]);
extern void LcdCursor(uint8_t);

void InitZeile(uint8_t);
void StartTimer3(void);
void StopTimer3(void);
void InitExtInt6(void);
void InitExtInt4(void);
/*****************************************************************************/

#define I2C_RP6_BASE_ADR 10		// The default address of the Slave Controller 
								// on the RP6 Mainboard

/*****************************************************************************/
// I2C Registers that can be read by the Master. Their names should 
// be self-explanatory and directly relate to the equivalent variables/functions 
// in the RP6Library

#define I2C_REG_STATUS1 		 0
#define I2C_REG_STATUS2 		 1
#define I2C_REG_MOTION_STATUS 	 2
#define I2C_REG_POWER_LEFT 		 3
#define I2C_REG_POWER_RIGHT 	 4
#define I2C_REG_SPEED_LEFT 		 5
#define I2C_REG_SPEED_RIGHT 	 6
#define I2C_REG_DES_SPEED_LEFT 	 7
#define I2C_REG_DES_SPEED_RIGHT  8
#define I2C_REG_DIST_LEFT_L 	 9
#define I2C_REG_DIST_LEFT_H 	 10
#define I2C_REG_DIST_RIGHT_L     11
#define I2C_REG_DIST_RIGHT_H 	 12
#define I2C_REG_ADC_LSL_L 		 13
#define I2C_REG_ADC_LSL_H 		 14
#define I2C_REG_ADC_LSR_L 		 15
#define I2C_REG_ADC_LSR_H 		 16
#define I2C_REG_ADC_MOTOR_CURL_L 17
#define I2C_REG_ADC_MOTOR_CURL_H 18
#define I2C_REG_ADC_MOTOR_CURR_L 19
#define I2C_REG_ADC_MOTOR_CURR_H 20
#define I2C_REG_ADC_UBAT_L 		 21
#define I2C_REG_ADC_UBAT_H 		 22
#define I2C_REG_ADC_ADC0_L 		 23
#define I2C_REG_ADC_ADC0_H 		 24
#define I2C_REG_ADC_ADC1_L 		 25
#define I2C_REG_ADC_ADC1_H 		 26
#define I2C_REG_RC5_ADR	 		 27
#define I2C_REG_RC5_DATA	 	 28
#define I2C_REG_LEDS	 		 29

#define Zeilenlaenge 21
#define BlankByte 32
char LcdZeile[Zeilenlaenge];

/*****************************************************************************/
uint8_t counter = 0;

ISR(TIMER3_OVF_vect)
{
//  writeString_P("\n****************** Timer Interrupt erkannt *****************\n");
//  DDRC |= (1<<DDC3); //led zum test
  if(PORTC & (1<<PC3))
  {
writeString_P("\nLED aus\n");
     PORTC &= ~(1<<PC3);
  }
  else
  {
writeString_P("\nLED ein\n");
	 PORTC |= (1<<PC3);
  }
}

ISR(INT6_vect)
{
//  writeString_P("\n****************** Interrupt erkannt *****************\n");
  StartTimer3();
  DDRC |= (1<<DDC2); //led zum test
  PORTC |= (1<<PC2);
}

ISR(INT7_vect)
{
	writeString_P("\ni7\n");
	PORTC |= (1<<PC1);
}

ISR(INT4_vect)
{
	char OutStr[10];
	if(TCCR3B & (1<<CS32))
	{
    StopTimer3();	
	uint16_t tm = TCNT3H;//+TCNT3L;
	sprintf(OutStr,"%d",tm);
    writeString_P("\n****************** Interrupt erkannt *****************\n");
	writeString(OutStr);
	writeString_P("#\n");
	writeInteger(tm, DEC);

	writeString_P_WIFI("  --  ");
		writeInteger_WIFI(TCNT3H, DEC);
		writeString_P_WIFI("  --  ");
		writeInteger_WIFI(TCNT3L, DEC);

	writeString_P("\n");
  PORTC &= ~(1<<PC2);   //LED
  }
}


void InitExtInt4(void)
{
	EIMSK |= (1<< INT4);		// Enable Ext Int 4
    EICRB |= (1<< ISC40);		// falling edge
}

void InitExtInt6(void)
{
	EIMSK |= (1<< INT6);		// Enable Ext Int 6
    EICRB |= (1<< ISC60);		// falling edge
}

void StartTimer3(void)
{
//    TCNT3L = TCNT3H = 0;		// Z�hler l�schen
	TCCR3B |= (1<<CS32);
    TIMSK3 |= (1<<TOIE3);		// Interrupts aktivieren und damit Timer starten

	EIMSK |= (1<< INT7);		// Enable Ext Int 7
    EICRB |= (1<< ISC70);		// falling edge
}

void StopTimer3(void)
{
    TIMSK3 &= ~(1<<TOIE3);		// Interrupts aktivieren und damit Timer starten
	TCCR3B &= ~(1<<CS32);		// prescaler
//    TCNT3L = TCNT3H = 0;		// Z�hler l�schen
}

void CloseConnection()
{
	// Wait a bit and then close connection:
	setLEDs(0b0011);
	mSleep(100);  // Depending on the content you may have to wait longer. 			
	writeString("\nClose connection... ");
	enter_cmd_mode_WIFI();
	writeCommand_WIFI("close\r");
	leave_cmd_mode_WIFI();
	writeString("done!\n");
	setLEDs(0b0000);	
}

void SendWebPageRepeat(uint8_t *RP6Regs, char *Param)
{
	uint16_t UBat = RP6Regs[I2C_REG_ADC_UBAT_H]*256 + RP6Regs[I2C_REG_ADC_UBAT_L];
	uint8_t UBatV = UBat / 100;
	uint8_t UBatN = UBat % 100;
	uint16_t UAdc0 = RP6Regs[I2C_REG_ADC_ADC0_H]*256 + RP6Regs[I2C_REG_ADC_ADC0_L];
							
	// ***************************************************************** 
	// The content: 
	// Send HTTP Header:
	writeString_P_WIFI("HTTP/1.1 200 OK\r\n"); 
	writeString_P_WIFI("Content-Type: text/html\r\n"); 
	writeString_P_WIFI("\r\n");
	writeString_P_WIFI("<meta http-equiv=\"refresh\" content=\"1;url=http://192.168.178.171:2000\">");
	writeString_P_WIFI("<html><head><title></title>");
/*					
	writeString_P_WIFI("<script type=\"text/javascript\">");
	writeString_P_WIFI("function PostData(){");
	writeString_P_WIFI("<form method=\"POST\">");
	writeString_P_WIFI("</form>");
	writeString_P_WIFI("}");
	writeString_P_WIFI("</script>");
*/				
	writeString_P_WIFI("Robo Betriebsdaten:</head><body>");

	writeString_P_WIFI("<br></br>");

	writeString_P_WIFI("<table style=\"width: 20%;\"><tr><td>");
	writeString_P_WIFI("UBat:");
	writeString_P_WIFI("</td>");
	writeString_P_WIFI("<td>");
	writeInteger_WIFI(UBatV, DEC);
	writeString_P_WIFI(".");
	writeInteger_WIFI(UBatN, DEC);
	writeString_P_WIFI("</td>");
	writeString_P_WIFI("</tr>");
				
	writeString_P_WIFI("<tr><td>");
	writeString_P_WIFI("SW Sensor ADC Ticks:");
	writeString_P_WIFI("</td>");
	writeString_P_WIFI("<td>");
	writeInteger_WIFI(UAdc0, DEC);
	writeString_P_WIFI("</td>");
	writeString_P_WIFI("</tr>");
	
	writeString_P_WIFI("<tr><td>");
	writeString_P_WIFI("Leistung Motor Rechts:");
	writeString_P_WIFI("</td>");
	writeString_P_WIFI("<td>");
	writeInteger_WIFI(RP6Regs[I2C_REG_POWER_RIGHT], DEC);
	writeString_P_WIFI("</td>");
	writeString_P_WIFI("</tr>");
	
	writeString_P_WIFI("<tr><td>");
	writeString_P_WIFI("Leistung Motor Links:");
	writeString_P_WIFI("</td>");
	writeString_P_WIFI("<td>");
	writeInteger_WIFI(RP6Regs[I2C_REG_POWER_LEFT], DEC);
	writeString_P_WIFI("</td>");
	writeString_P_WIFI("</tr>");				
	writeString_P_WIFI("</table>");
	
	writeString_P_WIFI("<br></br>");
	writeString_P_WIFI("<br></br>");	
	writeString_P_WIFI("<form action=\"\" method=\"get\">");
	writeString_P_WIFI("<input name=\"senden\" type=\"submit\" value=\"Vorgabe\"\"/>");
	writeString_P_WIFI("</form>");		
				
	writeString_P_WIFI("<br></br>");
	writeString_P_WIFI("</br>");
	writeString_P_WIFI("<br></br>");
	writeString_WIFI(receiveBuffer_WIFI);
	writeString_P_WIFI("<br></br>");				
				
	writeString_P_WIFI("</body></html>");					
}



void SendWebPageEingabe(uint8_t *RP6Regs)
{
	writeString_P_WIFI("HTTP/1.1 200 OK\r\n"); 
	writeString_P_WIFI("Content-Type: text/html\r\n"); 
	writeString_P_WIFI("\r\n");
//	writeString_P_WIFI("<meta http-equiv=\"refresh\" content=\"3;url=http://192.168.178.171:2000\">");
	writeString_P_WIFI("<html><head><title></title>");	
	writeString_P_WIFI("Robo Vorgaben</head><body>");

	writeString_P_WIFI("<br></br>");
	writeString_P_WIFI("<br></br>");

	writeString_P_WIFI("<form action=\"\" method=\"get\">");	
	writeString_P_WIFI("<table style=\"width: 35%;\"><tr><td>");
	writeString_P_WIFI("Leistung Motor Rechts:");
	writeString_P_WIFI("</td>");
	writeString_P_WIFI("<td>");	
	writeString_P_WIFI("<input name=\"PRechts\" type=\"text\" value=");
	writeInteger_WIFI(RP6Regs[I2C_REG_POWER_RIGHT],DEC);
	writeString_P_WIFI(" />");	
	writeString_P_WIFI("</td>");
	writeString_P_WIFI("</tr>");
		
	writeString_P_WIFI("<tr><td>");
	writeString_P_WIFI("Leistung Motor Links:");
	writeString_P_WIFI("</td>");
	writeString_P_WIFI("<td>");
	writeString_P_WIFI("<input name=\"PLinks\" type=\"text\" value=");
	writeInteger_WIFI(RP6Regs[I2C_REG_POWER_LEFT],DEC);
	writeString_P_WIFI(" />");
	writeString_P_WIFI("</td>");
	writeString_P_WIFI("</tr>");
		
	writeString_P_WIFI("</table>");
	writeString_P_WIFI("<br></br>");
	writeString_P_WIFI("<br></br>");
	writeString_P_WIFI("<input name=\"senden\" type=\"submit\" value=\"Absenden!\"\"/>");
	writeString_P_WIFI("</form>");

	writeString_P_WIFI("<br></br>");
	writeString_WIFI(receiveBuffer_WIFI);
						
	writeString_P_WIFI("</body></html>");
}

/*
* The most simple "Webserver" you can think of. 
*/
void task_simple_webserver(uint8_t *RP6Regs)
{
	char Param[100];
	char PRechts[8] = {'P','R','e','c','h','t','s',0};
	char PLinks[7] =  {'P','L','i','n','k','s', 0};
		
	if(getBufferLength_WIFI()) { 	// Do we have data?					

		if(parseLine_WIFI(readChar_WIFI()))  // Did we receive a full text line?
		{									 // This also stores the text line in receiveBuffer_WIFI
			setLEDs(0b1000);
			writeString("Got request: ");
			writeString(receiveBuffer_WIFI);
			if (strstr(receiveBuffer_WIFI,"GET /")!=NULL) // Check for GET Command
			{
				writeString("\nGOT GET!\n");
				setLEDs(0b1100);
				mSleep(50);
				if (strstr(receiveBuffer_WIFI,"Vorgabe")!=NULL)
				{
					SendWebPageEingabe(RP6Regs);
					CloseConnection();
				}
				else
				{
					if (strstr(receiveBuffer_WIFI,"Absenden")!=NULL)
					{
						GetHttpParam(receiveBuffer_WIFI, PRechts, Param);
						writeString_P("\n**********xxxxxxxxxxxxxx*******\n");
						writeString(Param);
						writeString_P("\n");
						RP6Regs[I2C_REG_POWER_RIGHT]= atoi(Param);
						
						GetHttpParam(receiveBuffer_WIFI, PLinks, Param);
						writeString_P("\n**********xxxxxxxxxxxxxx*******\n");
						writeString(Param);
						writeString_P("\n");
						RP6Regs[I2C_REG_POWER_LEFT]= atoi(Param);
					}
					SendWebPageRepeat(RP6Regs, Param);										
				}				
			}
			else
			{
				if (strstr(receiveBuffer_WIFI,"POST /")!=NULL) // Check for Post Command
				{
					writeString_P("\n***************** Post empfangen\n");	
					writeString(receiveBuffer_WIFI);
					CloseConnection();
				}
				else 
				{
					writeString(receiveBuffer_WIFI);
					if (strstr(receiveBuffer_WIFI,"Cache-Control")!=NULL)
					{
						writeString_P("\n***************** Was war das?\n");
					}
									
					writeString_P_WIFI("\nI am simple. I don't know.\n");
					writeString_P("\nI am simple. I don't know.\n");					
					CloseConnection();
				}
			}							
		}
	}
}

void WriteUBat(uint16_t UBat)
{
	writeString_P("\nUBat: ");
	writeInteger(UBat, DEC);
	writeString_P(" Volt\n");
}

void transmissionErrorHandler(uint8_t errorState)
{
	writeString_P("\nI2C ERROR - TWI STATE: 0x");
	writeInteger(errorState, HEX);
	writeChar('\n');
	
	writeString_P_WIFI("\nI2C ERROR - TWI STATE: 0x");
	writeInteger_WIFI(errorState, HEX);
	writeChar_WIFI('\n');
}

void InitZeile(uint8_t InitCharacter)
{
    uint8_t i;
    for(i=0; i<Zeilenlaenge-1; i++)
    {
          LcdZeile[i] = InitCharacter;
    }
	LcdZeile[Zeilenlaenge-1] = 0;
}

void readAllRegisters(uint8_t *RP6data)
{
	I2CTWI_transmitByte(I2C_RP6_BASE_ADR, 0); // Start with register 0...
	I2CTWI_readBytes(I2C_RP6_BASE_ADR,RP6data, 30); // and read all 30 registers up to
													// register Number 29 !
}

/** 
 * Here we demonstrate how to read a few specific registers. 
 * It is just the same as above, but we only read 4 registers and
 * start with register Number 13.
 * We also show how to combine values from high and low registers 
 * back together to a 16 Bit value.
 */
void readLightSensors(void)
{
	uint8_t lightSens[4];

	I2CTWI_transmitByte(I2C_RP6_BASE_ADR, 13); // Start with register 13 (LSL_L)...
	I2CTWI_readBytes(I2C_RP6_BASE_ADR, lightSens, 4); // and read all 4 registers up to
													// register Number 16 (LSR_H) !
/*	writeString_P_WIFI("Light Sensor registers:\n");
	writeString_P_WIFI(" | LSL_L:"); writeInteger_WIFI(lightSens[0], DEC); 
	writeString_P_WIFI(" | LSL_H:"); writeInteger_WIFI(lightSens[1], DEC); 
	writeString_P_WIFI(" | LSR_L:"); writeInteger_WIFI(lightSens[2], DEC); 
	writeString_P_WIFI(" | LSR_H:"); writeInteger_WIFI(lightSens[3], DEC); 

	writeString_P_WIFI("\n\n Light Sensor Values:");
	writeString_P_WIFI(" | LSL:"); writeInteger_WIFI(lightSens[0] + (lightSens[1]<<8), DEC); 
	writeString_P_WIFI(" | LSR:"); writeInteger_WIFI(lightSens[2] + (lightSens[3]<<8), DEC); 
	writeChar_WIFI('\n');
*/
}

int main(void)
{
	uint16_t uADC0 = 0;
	uint16_t uADC010 = 0;
	uint16_t ubat10 = 0;
	uint16_t UBat=0;
	uint8_t Flag10ms = 0;
	uint8_t Flag100ms = 0;
	uint16_t Counter100ms = 0;
	uint8_t RP6Regs[32];
	
	initRP6M256();    // Always call this first! The Processor will not work
					  // correctly otherwise. 
	// Start the stopwatches:
	startStopwatch1(); // Stopwatch1 starten!
	setStopwatch2(10); // Stopwatch2 auf 10ms setzen	
	startStopwatch2(); // Stopwatch2 starten!
	setStopwatch2(1000); // Stopwatch2 auf 1000ms setzen	
	
	I2CTWI_initMaster(100);
	I2CTWI_setTransmissionErrorHandler(transmissionErrorHandler);
	task_I2CTWI(); // Call I2C Management routine

	LcdInit(0x40);
	ClearDisplay(0x40);
	LcdCursor(0);

	// Make sure WLAN Module Packet size / buffer size and flush timer is OK
	enter_cmd_mode_WIFI();   
	issueCMD_WIFI("set comm size 1024","AOK"); 
	issueCMD_WIFI("set comm time 10","AOK"); 
	leave_cmd_mode_WIFI();

    InitExtInt6();
	InitExtInt4();

	while(true) 
	{
		if(TCNT3H !=0 )
		{
		   writeString_P("ok!\n");
		}
		if(getStopwatch1() >= 10) // Sind 10ms vergangen?
		{
			setStopwatch1(0); // Stopwatch2 auf 0 zur�cksetzen
			Flag10ms = 1;
		}
		if(Flag10ms > 0)
		{
			if(++Counter100ms >= 10)
			{
				Flag100ms = 1;
				Counter100ms = 0;
			}
			Flag10ms = 0;
		}
		
		if(Flag100ms == 1)
		{
			Flag100ms = 0;
			readAllRegisters(RP6Regs);
			uADC0 = RP6Regs[I2C_REG_ADC_ADC0_H]*256 + RP6Regs[I2C_REG_ADC_ADC0_L];
			uADC010 = uADC0- (uADC0/100)*100;
			
			InitZeile(BlankByte);
			if(uADC010 < 10)
			{
				sprintf(LcdZeile,"UADC0: %d.0%d V",uADC0/100,uADC010);
			}
			else
			{
				sprintf(LcdZeile,"UADC0: %d.%d V",uADC0/100,uADC010);
			}			
			WriteLcdZeile4(BlankByte,LcdZeile);		}
		
		if(getStopwatch2() > 1000) // Sind 1000ms (= 1s) vergangen?
		{
			setStopwatch2(0); // Stopwatch2 auf 0 zur�cksetzen

			UBat = RP6Regs[I2C_REG_ADC_UBAT_H]*256 + RP6Regs[I2C_REG_ADC_UBAT_L];
//			WriteUBat(UBat);

			ubat10 = UBat- (UBat/100)*100;
			InitZeile(BlankByte);
//			sprintf(LcdZeile,"UBat: %d0 mV",UBat);
//			WriteLcdZeile1(BlankByte,LcdZeile);
			if(ubat10 < 10)
			{
				sprintf(LcdZeile,"UBat: %d.0%d V",UBat/100,ubat10);
			}
			else
			{
				sprintf(LcdZeile,"UBat: %d.%d V",UBat/100,ubat10);
			}
			WriteLcdZeile2(BlankByte,LcdZeile);

//			LcdCursor(1);		
		}
		task_I2CTWI(); // Call I2C Management routine
		task_simple_webserver(RP6Regs);	
	}
	return 0;
}
